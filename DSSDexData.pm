package DSSDexData;

use v5.20;

use Moose;
use Tk;
use Data::Dumper;

my $tk = MainWindow->new();

use Win32::OLE qw(in with);
use Win32::OLE::Const 'Microsoft Excel';
use Win32::OLE::Variant;
use Win32::OLE::NLS qw(:LOCALE :DATE);

use XML::Simple qw(:strict);

$Win32::OLE::Warn = 3;

has 'excel_file_name' => (
	is => 'ro',
	lazy => 1,
	default => sub { return $tk->getOpenFile( -title => 'Open Excel data file' ) },
);

has '_excel_workbook' => (
	is => 'ro',
	lazy => 1,
	builder => '_open_excel_file',
	init_arg => undef,
);

has '_worksheet' => (
	is => 'ro',
	lazy => 1,
	builder => '_get_worksheet',
	init_arg => undef,
);

has '_last_column' => (
	is => 'ro',
	lazy => 1,
	builder => '_get_last_column',
	init_arg => undef,
);

has '_field_names' => (
	is => 'ro',
	lazy => 1,
	builder => '_get_field_names',
	init_arg => undef,
);

has '_current_row' => (
	is => 'rw',
	isa => 'Int',
	default => 1,
);

has 'xml_name' => (
	is => 'ro',
	lazy => 1,
	default => sub { $tk->getSaveFile( -title => 'Save XML file', -defaultextension => 'xml' ) },
);
	





###########
# Methods #
###########

sub export_xml {
	my ( $self ) = @_;
	
	my $combined_data = [
		{
			key => 'Clients',
			value => [],
		},
		{
			key => 'Cases',
			value => [],
		}
	];
	
	while ( my $row = $self->_get_next_row_as_hash() ) {
		push @{ $combined_data->[0]{value} }, { key => 'Client', value => $row->{Client} };
		push @{ $combined_data->[1]{value} }, { key => 'Case', value => $row->{Case} };
	}
	
	open my $xml_out, '>', $self->xml_name();
	
	say $xml_out '<?xml version="1.0" encoding="utf-8"?>';
	say $xml_out '<DEXFileUpload>';
	print_xml_fields($xml_out, $combined_data, '');
	say $xml_out '</DEXFileUpload>';
	
#	XMLout( $combined_data,
#		OutputFile => $self->xml_name(),
#		KeyAttr => [],
#		SuppressEmpty => 1,
#		GroupTags => {
#			Clients		=> 'Client',
#			Cases		=> 'Case',
#			Sessions	=> 'Session',
#			ClientAssessments	=> 'ClientAssessment',
#			SessionAssessments	=> 'SessionAssessment',
#		},
#		RootName => 'DEXFileUpload',
#		XMLDecl => '<?xml version="1.0" encoding="utf-8"?>',
#	);
}

sub _open_excel_file {
	my $self = shift;
	
	my $Excel = Win32::OLE->new('Excel.Application') or die "oops\n";
	$Excel->{DisplayAlerts} = 0;
	
	return $Excel->Workbooks->Open( $self->excel_file_name() );
}

sub _get_field_names {
	my $self = shift;
	
	my $sheet = $self->_worksheet();
	
	$self->_current_row(1);
	
	return $self->_get_next_row();
}

sub _get_worksheet {
	my $self = shift;
	
	my $book = $self->_excel_workbook();
	
	my $sheet = $book->Worksheets("Sheet1");
	$sheet->Activate();
	
	return $sheet;
}

sub _get_last_column {
	my $self = shift;
	my $sheet = $self->_worksheet();
	
	my $last_column;
	
	for ( 'A'..'ZZ' ) {
		last unless $sheet->Range($_ . '1')->{Value};
		$last_column = $_;
	}
	
	return $last_column;
}

sub _get_next_row {
	my $self = shift;
	
	my $row_num = $self->_current_row();
	my $sheet = $self->_worksheet();
	
	my $row = [map { $sheet->Range($_)->{Value} }
				map { $_ . $row_num } ( 'A' .. $self->_last_column() )
			];
	
	$self->_current_row($row_num + 1);
	
	return $row;
}

sub print_xml_fields {
	my ( $fh, $array, $indent ) = @_;
	$indent = '' unless $indent;
	
	foreach my $pair ( @$array ) {
		#print Dumper $pair;
		my $key = $pair->{key};
		my $value = $pair->{value};
		
		print $fh "$indent<$key>";
		
		if ( ref $value ) {
			print $fh "\n";
			print_xml_fields($fh, $value, "$indent  ");
			print $fh $indent;
		}
		else {
			print $fh "$value";
		}
		
		print $fh "</$key>\n";
	}
}

sub _get_next_row_as_hash {
	my $self = shift;
	
	my $field_names = $self->_field_names();
	my $row_array = $self->_get_next_row();
	return undef unless grep {$_} @$row_array;
	my $row_temp_hash = { map { ( $field_names->[$_] => $row_array->[$_] ) } ( 0 .. @{$field_names} - 1 ) };
	
	my $get_fields = sub { __get_fields( $row_temp_hash, @_ ) };
	
	$row_temp_hash->{ResidentialAddress} = $get_fields->( qw{
		AddressLine1
		AddressLine2
		Suburb
		StateCode
		Postcode
	});
	
	$row_temp_hash->{ReasonForAssistance} = $get_fields->( qw{
		ReasonForAssistanceCode
		IsPrimary
	});
	
	$row_temp_hash->{ParentingAgreementOutcome} = $get_fields->( qw{
		ParentingAgreementOutcomeCode
		DateOfParentingAgreement
		DidLegalPractitionerAssistWithFormalisingAgreement
	});
	
	$row_temp_hash->{Section60I} = $get_fields->( qw{
		Section60ICertificateTypeCode
		DateIssued
	});
	
	$row_temp_hash->{ReasonsForAssistance} = $get_fields->( 'ReasonForAssistance' );
	
	$row_temp_hash->{CaseClient} = $get_fields->( qw{
		ClientId
		ReferralSourceCode
		ReasonsForAssistance
		ExitReasonCode
	});
	
	$row_temp_hash->{CaseClients} = $get_fields->( 'CaseClient' );
	
	$row_temp_hash->{PurposeCodes} = $get_fields->( 'PurposeCode' );
	
	$row_temp_hash->{Referral} = $get_fields->( qw{
		TypeCode
		PurposeCodes
	});
	
	$row_temp_hash->{ClientReferralOutWithPurpose} = $get_fields->( 'Referral' );
	
	$row_temp_hash->{SessionClient} = $get_fields->( qw{
		ClientId
		ParticipationCode
		ClientReferralOutWithPurpose
	});
	
	$row_temp_hash->{SessionClients} = $get_fields->( 'SessionClients' );
	
	$row_temp_hash->{Scores} = $get_fields->( 'ScoreCode' );
	
	$row_temp_hash->{Assessment} = $get_fields->( qw{
		ScoreTypeCode
		AssessmentPhaseCode
		Scores
	});
	
	$row_temp_hash->{Assessments} = $get_fields->( 'Assessment' );
	
	my $row;
	
	$row->{Client} = $get_fields->( qw{
		ClientId
		Slk
		ConsentToProvideDetails
		ConsentedForFutureContacts
		GivenName
		FamilyName
		IsUsingPsuedonym
		BirthDate
		IsBirthDateAnEstimate
		GenderCode
		CountryOfBirthCode
		LanguageSpokenAtHomeCode
		AboriginalOrTorresStraitIslanderOriginCode
		HasDisabilities
		Disabilities
		AccommodationTypeCode
		DVACardStatusCode
		HasCarer
		ResidentialAddress
		IsHomeless
		HouseholdCompositionCode
		MainSourceOfIncomeCode
		IncomeFrequencyCode
		IncomeAmount
		FirstArrivalYear
		FirstArrivalMonth
		MigrationVisaCategoryCode
		AncestryCode
	});
	$row->{Case} = $get_fields->( qw{
		CaseId
		OutletActivityId
		TotalNumberOfUnidentifiedClients
		CaseClients
	});
	$row->{Session} = $get_fields->( qw{
		SessionId
		CaseId
		SessionDate
		ServiceTypeId
		TotalNumberOfUnidentifiedClients
		FeesCharged
		MoneyBusinessCommunityEducationWorkshopCode
		InterpreterPresent
		SessionClients
		TimeMinutes
		TotalCost
		Quantity
	});
	$row->{ClientAssessment} = $get_fields->( qw{
		ClientId
		CaseId
		SessionId
		Assessments
	});
	$row->{SessionAssessment} = $get_fields->( qw{
		CaseId
		SessionId
		Assessments
	});
	
	return $row;
}

sub __get_fields {
	my $row_hash = shift;
	
	return [grep { $_->{value} or ( not ref $_->{value} and defined $_->{value} ) } map { { key => $_, value => $row_hash->{$_} } } ( @_ )];
}

no Moose;
__PACKAGE__->meta->make_immutable;
1;







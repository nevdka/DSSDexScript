Script to convert data from excel to xml to help my dad do a bulk upload to a
government site.

This script is written for perl 5.20 but will probably work in previous versions.
It slurps the excel file into memory, then outputs as xml, following the schema
provided by the government department.